# README #

This repo was created aty olaf's request to get started with dash (and as an example of how to read the maxmine api from python)


### Setup ###

* I started with the dash tutorial installation (https://plot.ly/dash/installation)
* I then put all the python module requirements from this (and one I knew were were going to used called requests) in a file called requirements.txt
* I also started a file called dev-requirements.txt which list the things I need to develop, not run, the code (here it's currently only ipython -> py.test might come as I add tests.
* I suggest you set this up in a virtual environment (virtualenv) to separate out this project from your whole computer - there might be different requirements for different projects or installing this might overwrite something critical on your computer for normal operation (eeekkk!)
* http://sourabhbajaj.com/mac-setup/Python/virtualenv.html is an reference to virtualenv on a mac (I use a wrapper for this on my machine calle direnv - don't use this to start with!!!)
* activate your virtualenv (if you follow the instructions above it would be the command ```source venv/bin/activate```
* install requirements ```pip install -r requirements```
* also install dev requirements the same way - I find ipython nice and interactive - you could also use pycharm as an ide - it's what we use.
* once this is done ```python app.py``` will run the app - follow instructions

### Where to from here ###

* I think Ben and I would like to follow along and help as much as we can.... this took me 15 mins to change the base app to plot our data/ and it was fun
* the best way to help would probably be to push to git and ask questions... (maybe on our own board here???)
* I'm pretty keen to help you succeed here as I think this is much better for us that Power BI - there is no objective evidence for this other than I understand this as it is very close to what we already do.
