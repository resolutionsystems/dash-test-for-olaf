# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go

import requests
from collections import Counter
import os

MAXMINE_USERNAME = os.environ.get('MAXMINE_USERNAME')
MAXMINE_PASSWORD = os.environ.get('MAXMINE_PASSWORD')
# get data from the API
r=requests.get(
    'https://wolvekrans.south32.max-mine.com/custom/south32/cyclesummaries/beta1?shiftId=201710300',
    auth=(MAXMINE_USERNAME, MAXMINE_PASSWORD)
)

data=r.json()

haul_truck_counts=Counter([x['HaulTruckModel'] for x in data['LoadingEvents']])

app = dash.Dash()

app.layout = html.Div(children=[
    html.H1(children='Hello Dash'),

    html.Div(children='''
        Dash: A web application framework for Python.
    '''),
    dcc.Graph(
        figure=go.Figure(
            data=[
                go.Bar(
                    x= list(haul_truck_counts.keys()),
                    y= list(haul_truck_counts.values()),
                    name='Rest of world',
                    marker=go.Marker(
                        color='rgb(55, 83, 109)'
                    )
                ),
            ],
            layout=go.Layout(
                title='US Export of Plastic Scrap',
                showlegend=True,
                legend=go.Legend(
                    x=0,
                    y=1.0
                ),
                margin=go.Margin(l=40, r=0, t=40, b=30)
            )
        ),
        style={'height': 300},
        id='my-graph'
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)
